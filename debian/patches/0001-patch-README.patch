From: Sean Whitton <spwhitton@spwhitton.name>
Date: Sun, 13 Dec 2015 10:11:12 -0700
Subject: patch README.md & extensions.md for Debian
Forwarded: not-needed

---
 README.md         | 34 +++++-----------------------------
 doc/extensions.md |  6 ++++--
 2 files changed, 9 insertions(+), 31 deletions(-)

--- a/README.md
+++ b/README.md
@@ -1,12 +1,5 @@
 # Projectile
 
-[![Build Status](https://github.com/bbatsov/projectile/workflows/CI/badge.svg)](https://github.com/bbatsov/projectile/actions?query=workflow%3ACI)
-[![MELPA](http://melpa.org/packages/projectile-badge.svg)](http://melpa.org/#/projectile)
-[![MELPA Stable](http://stable.melpa.org/packages/projectile-badge.svg)](http://stable.melpa.org/#/projectile)
-[![NonGNU ELPA](https://elpa.nongnu.org/nongnu/projectile.svg)](https://elpa.nongnu.org/nongnu/projectile.html)
-[![License GPL 3][badge-license]](http://www.gnu.org/licenses/gpl-3.0.txt)
-[![Discord](https://img.shields.io/badge/chat-on%20discord-7289da.svg?sanitize=true)](https://discord.gg/3Cf2Qpyry5)
-
 ## Synopsis
 
 **Projectile** is a project interaction library for Emacs. Its goal is to
@@ -59,42 +52,17 @@ You can support my work on Projectile vi
  [Patreon](https://www.patreon.com/bbatsov) and
  [GitHub Sponsors](https://github.com/sponsors/bbatsov).
 
-## Projectile in Action
-
-Here's a glimpse of Projectile in action (using `ivy`):
-
-![Projectile Demo](doc/modules/ROOT/assets/images/projectile-demo.gif)
-
-In this short demo you can see:
-
-* finding files in a project
-* switching between implementation and test
-* switching between projects
-
 ## Quickstart
 
 The instructions that follow are meant to get you from zero to a running Projectile setup
 in a minute.  Visit the
-[online documentation](https://docs.projectile.mx) for (way) more
+[user manual](https://docs.projectile.mx) (or
+[here](/usr/share/doc/elpa-projectile/nav.html)) for (way) more
 details.
 
 ### Installation
 
-`package.el` is the built-in package manager in Emacs.
-
-Projectile is available on all major `package.el` community
-maintained repos - [NonGNU ELPA](https://elpa.nongnu.org),
-[MELPA Stable](http://stable.melpa.org)
-and [MELPA](http://melpa.org).
-
-You can install Projectile with the following command:
-
-<kbd>M-x</kbd> `package-install` <kbd>[RET]</kbd> `projectile` <kbd>[RET]</kbd>
-
-Alternatively, users of Debian 9 or later or Ubuntu 16.04 or later may
-simply `apt-get install elpa-projectile`.
-
-Finally add this to your Emacs config:
+Add this to your Emacs config:
 
 ```elisp
 (projectile-mode +1)
@@ -110,7 +78,8 @@ Those keymap prefixes are just a suggest
 
 Enable `projectile-mode`, open a file in one of your projects and type a command such as <kbd>C-c p f</kbd>.
 
-See the [online documentation](https://docs.projectile.mx) for more details.
+See the [user manual](https://docs.projectile.mx) (or
+[here](/usr/share/doc/elpa-projectile/nav.html)) for more details.
 
 To get the most of Projectile you also need to enable (and potentially install) some minibuffer completion framework (e.g. `ido`, `ivy` or `selectrum`). See [this section](https://docs.projectile.mx/projectile/configuration.html#completion-options) of the documentation for more details.
 
@@ -143,7 +112,7 @@ Joining this esteemed group of people is
 
 ## Changelog
 
-A fairly extensive changelog is available [here](CHANGELOG.md).
+A fairly extensive changelog is available [here](NEWS.gz).
 
 [badge-license]: https://img.shields.io/badge/license-GPL_3-green.svg
 
--- a/doc/modules/ROOT/pages/extensions.adoc
+++ b/doc/modules/ROOT/pages/extensions.adoc
@@ -3,16 +3,16 @@
 There are a number of packages that built on top of the basic functionality provided by Projectile:
 
 * https://github.com/ericdanan/counsel-projectile[counsel-projectile] provides Ivy integration
-* https://github.com/bbatsov/helm-projectile[helm-projectile] provides Helm integration
-* https://github.com/bbatsov/persp-projectile[persp-projectile] provides perspective.el integration
+* https://github.com/bbatsov/helm-projectile[helm-projectile] provides Helm integration (`apt install elpa-helm-projectile`)
+* https://github.com/bbatsov/persp-projectile[persp-projectile] provides perspective.el integration (`apt install elpa-persp-projectile`)
 * https://github.com/asok/projectile-rails[projectile-rails] provides extra functionality for Ruby on Rails projects
 * https://github.com/IvanMalison/org-projectile[org-projectile] provides functions for the creation of org-mode TODOs that are associated with Projectile projects.
-* https://github.com/Alexander-Miller/treemacs/blob/master/src/extra/treemacs-projectile.el[treemacs-projectile] provides integration between treemacs and Projectile.
+* https://github.com/Alexander-Miller/treemacs/blob/master/src/extra/treemacs-projectile.el[treemacs-projectile] provides integration between treemacs and Projectile (`apt install elpa-treemacs-projectile`).
 * https://github.com/anshulverma/projectile-speedbar[projectile-speedbar] provides integration between speedbar and Projectile.
 
 NOTE: MELPA lists https://melpa.org/#/?q=projectile[20 more Projectile extensions], but I'm too lazy to list them all here.
 
 There are also some packages that will use Projectile if present. Here are a few examples:
 
-* https://github.com/jaypei/emacs-neotree[neotree]
-* https://github.com/emacs-lsp/lsp-mode[lsp-mode]
+* https://github.com/jaypei/emacs-neotree[neotree] (`apt install elpa-neotree`)
+* https://github.com/emacs-lsp/lsp-mode[lsp-mode] (`apt install elpa-lsp-mode`)
